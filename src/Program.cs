using System;
using System.Threading.Tasks;
using demoMVC.DB;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace demoMVC
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BlogContext.GetBlogContext().Database.EnsureCreatedAsync();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
