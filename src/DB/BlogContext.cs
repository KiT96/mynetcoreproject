using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using src.Models;

namespace demoMVC.DB
{
    public class BlogContext : DbContext {
        private static BlogContext _instance;
        private static object syncLock = new object ();
        public BlogContext () { }

        protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder) {
            base.OnConfiguring (optionsBuilder);
            optionsBuilder.UseSqlServer ("Data Source=HSSSC1PCL01494\\KHANHIT;Initial Catalog=Blog;User ID=sa;Password=Khanhit96");
        }

        public DbSet<Blog> blogs { get; set; }

        public static BlogContext GetBlogContext () {
            if (_instance == null) {
                lock (syncLock) {
                    if (_instance == null) {
                        _instance = new BlogContext ();
                    }
                }
            }
            return _instance;
        }

        // get all blog from database
        public async Task<List<Blog>> GetList () {
            try {
                return await GetBlogContext ().blogs.ToListAsync ();
            } catch (System.Exception) {
                return null;
            }
        }

        public async Task<Blog> GetById(int Id){
            try
            {
                return await GetBlogContext().blogs.Where(blog => blog.BlogId == Id).FirstOrDefaultAsync();
            }
            catch (System.Exception)
            { 
                return null;
            }
        }
    }
}