using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using demoMVC.Models;
using Microsoft.EntityFrameworkCore;

namespace demoMVC.DB {
    public class FoodsContext : DbContext {
        private static FoodsContext _instance;
        private static object syncLock = new object ();
        public string sourceString { get; set; }

        public FoodsContext () {
            this.sourceString = "Data Source=HSSSC1PCL01494\\KHANHIT;Initial Catalog=Foods;User ID=sa;Password=Khanhit96";
        }

        protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder) {
            base.OnConfiguring (optionsBuilder);
            optionsBuilder.UseSqlServer (this.sourceString);
        }

        public DbSet<Foods> foods { get; set; }

        public static FoodsContext GetFoodsContext () {
            if (_instance == null) {
                lock (syncLock) {
                    if (_instance == null) {
                        _instance = new FoodsContext ();
                    }
                }
            }
            return _instance;
        }

        public async Task<bool> ConnectDatabase () {
            try {
                return await FoodsContext.GetFoodsContext ().Database.CanConnectAsync ();
            } catch (System.Exception) {
                return false;
            }
        }

        public async Task<List<Foods>> GetList () {
            try {
                return await GetFoodsContext ().foods.ToListAsync ();
            } catch (System.Exception) {
                return null;
            }
        }

        public async Task<bool> InsertFood (Foods food) {
            try {
                var context = FoodsContext.GetFoodsContext ();
                await context.foods.AddAsync (food);
                int rows = await context.SaveChangesAsync ();
                if (rows > 0) {
                    return true;
                } else {
                    return false;
                }
            } catch (System.Exception) {
                return false;
            }
        }

        public async Task<bool> UpdateFood (Foods food) {
            try {
                var context = FoodsContext.GetFoodsContext ();
                var result = await (from foo in context.foods where (foo.Id == food.Id) select foo).FirstOrDefaultAsync ();

                if (result != null) {
                    context.foods.Update (food);
                    int rows = await context.SaveChangesAsync ();
                    if (rows > 0) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } catch (System.Exception) {
                return false;
            }
        }

        public async Task<bool> DeleteFood (int Id) {
            try {
                var context = FoodsContext.GetFoodsContext ();
                var result = await (from foo in context.foods where (foo.Id == Id) select foo).FirstOrDefaultAsync ();

                if (result != null) {
                    context.foods.Remove (result);
                    int rows = await context.SaveChangesAsync ();
                    if (rows > 0) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } catch (System.Exception) {
                return false;
            }
        }

    }
}