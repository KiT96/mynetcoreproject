import { Component, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent {
  public blogs: Blog[];
  public http: HttpClient;
  public baseUrl: string;

  constructor(_http: HttpClient, @Inject("BASE_URL") _baseUrl: string) {
    this.http = _http;
    this.baseUrl = _baseUrl;
    this.getData()
  }

  getData = () => {
    this.http
      .get<Blog[]>(this.baseUrl + "api/blog/index")
      .subscribe(
        result => {
          console.log('result: ', result);
          this.blogs = result;
        },
        error => console.error("error: ", JSON.stringify(error))
      );
  };
}

interface Blog {
  blogid: number,
  title: string,
  thumbnail: string,
  writedate: string,
  summary: string
}
