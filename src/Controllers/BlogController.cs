using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using demoMVC.DB;
using Microsoft.AspNetCore.Mvc;
using src.Models;

namespace src.Controllers
{
    [Route ("api/[controller]/[action]")]
    [ApiController]
    public class BlogController : ControllerBase {
        public BlogContext _context { get; set; }
        public BlogController () {
            this._context = BlogContext.GetBlogContext ();
        }

        // GET api/blog
        [HttpGet]
        public async Task<ActionResult<List<Blog>>> Index () {
            return Ok (await this._context.GetList ());
        }

        // GET api/blog/5
        [HttpGet ("{id}")]
        public async Task<ActionResult<Blog>> Detail (int Id) {
            Console.WriteLine("resul: {0}", this._context.GetById(Id));
            return Ok(await this._context.GetById(Id));
        }

        // // POST api/blog
        // [HttpPost ("")]
        // public void Poststring (string value) { }

        // // PUT api/blog/5
        // [HttpPut ("{id}")]
        // public void Putstring (int id, string value) { }

        // // DELETE api/blog/5
        // [HttpDelete ("{id}")]
        // public void DeletestringById (int id) { }
    }
}