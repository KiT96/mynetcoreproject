using System.Collections.Generic;
using System.Threading.Tasks;
using demoMVC.DB;
using demoMVC.Models;
using Microsoft.AspNetCore.Mvc;

namespace demoMVC.Controllers {
    [Route ("api/[controller]/[action]")]
    [ApiController]
    public class FoodController : ControllerBase {
        public List<Foods> ListFood = new List<Foods> ();
        public FoodsContext _context { get; set; }

        public FoodController () {
            this._context = FoodsContext.GetFoodsContext ();
        }

        // GET api/food/getfoods
        [HttpGet]
        public async Task<ActionResult<List<Foods>>> GetFoods () => Ok(await this._context.GetList ());

        // GET api/food/addfoods
        [HttpPost]
        public async Task<ActionResult<bool>> AddFoods ([FromBody] Foods foods) {
            if (ModelState.IsValid) {
                return Ok (await this._context.InsertFood (foods));
            } else {
                return false;
            }
        }

        
        

    }
}