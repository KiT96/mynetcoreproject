using System;
using System.Collections.Generic;
using System.Linq;
using demoMVC.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace demoMVC.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class WeatherForecastController : ControllerBase {
        private static readonly string[] Summaries = new [] {
            "Freezing",
            "Bracing",
            "Chilly",
            "Cool",
            "Mild",
            "Warm",
            "Balmy",
            "Hot",
            "Sweltering",
            "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController (ILogger<WeatherForecastController> logger) {
            _logger = logger;
        }

        // GET api/weatherforecast
        [HttpGet]
        public ActionResult<IEnumerable<WeatherForecast>> Getstrings () {
            var rng = new Random ();
            return Enumerable.Range (1, Summaries.Length).Select (index => new WeatherForecast {
                    Date = DateTime.Now.AddDays (index),
                        TemperatureC = rng.Next (-20, 55),
                        Summary = Summaries[rng.Next (Summaries.Length)]
                })
                .ToArray ();
        }

        // GET api/weatherforecast/5
        [HttpGet ("{id}")]
        public ActionResult<WeatherForecast> GetstringById (int id) {
            return null;
        }

        // POST api/weatherforecast
        [HttpPost ("")]
        public void Poststring (string value) { }

        // PUT api/weatherforecast/5
        [HttpPut ("{id}")]
        public void Putstring (int id, string value) { }

        // DELETE api/weatherforecast/5
        [HttpDelete ("{id}")]
        public void DeletestringById (int id) { }
    }
}