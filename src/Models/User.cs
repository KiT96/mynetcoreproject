using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace src.Models {
    [Table("User")]
    public class User {
        
        [Key]
        public int UserId { get; set; }

        [Required]
        [StringLength(200)]
        public string UserName { get; set; }
    }
}