using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace src.Models {
    [Table("Blog")]
    public class Blog {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BlogId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Thumbnail { get; set; }

        [Required]
        public DateTime WriteDate { get; set; }

        [Required]
        public string Summary { get; set; }
    }
}