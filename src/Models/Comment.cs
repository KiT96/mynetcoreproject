using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace src.Models {
    [Table ("Comment")]
    public class Comment {
        [Key]
        public int CommentId { get; set; }

        [ForeignKey ("FK_COMMENT_BLOGID")]
        public int BlogId { get; set; }

        [ForeignKey ("FK_COMMENT_USERID")]
        public int UserId { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        public DateTime WriteDate { get; set; }
    }
}