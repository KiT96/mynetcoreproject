using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace demoMVC.Models {
    [Table("Foods")]
    public class Foods {
        public Foods (string Name, string Price) {
            this.Name = Name;
            this.Price = Price;
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        public string Price { get; set; }
    }
}